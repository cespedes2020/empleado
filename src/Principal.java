import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class Principal {

    //atributos
    static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    static Empleado [] listaEmpleados = new Empleado[10];
    public static void main(String[] args)  throws IOException{
        menu();
    }
    public static void menu() throws IOException{
        int opcion = 0;
        do {
            System.out.println("***Bienvenido al sistema Empleados***");
            System.out.println("1. Registrar un empleado");
            System.out.println("2. Lista de empleados");
            System.out.println("3. Salir");
            System.out.println("Digite una opcion : ");
            opcion = Integer.parseInt(br.readLine());
            //move to the ProcesarOpcion method with opcion
            procesarOpcion(opcion);
        }while(opcion !=3);
}
    public static void procesarOpcion(int opcion) throws IOException{
        switch (opcion){
            case 1: registraEmpleado();
                break;
            case 2: ImprimirEmpleados();
                break;
            case 3:
                System.out.println("Gracias por su visita");
                System.exit(0);
            default:
                System.out.println("Opcion invalida");

            }
        }

        public static void registraEmpleado() throws IOException{
            System.out.println("Ingrese la cedula: ");
            String cedula = br.readLine();
            System.out.println("Ingrese la nombre: ");
            String nombre = br.readLine();
            System.out.println("Ingrese la puesto: ");
            String puesto = br.readLine();
            Empleado empleado = new Empleado(cedula,nombre,puesto);
                for (int x = 0; x < listaEmpleados.length;x++){
                    if (listaEmpleados[x] == null){
                        listaEmpleados[x] = empleado;//asignamos empleado porque es el que estamos contruyendo y agregando
                        //esto hace que me llene bien la lista con empleados distintos
                        x = listaEmpleados.length;
                    }
                }
            }
            static public void ImprimirEmpleados(){
            for (int i = 0; i < listaEmpleados.length; i++){
                if (listaEmpleados[i] != null){
                    System.out.println(listaEmpleados[i].toString());
                }
            }
        }
    }
